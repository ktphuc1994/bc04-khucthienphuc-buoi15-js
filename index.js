/** BÀI 1:
 *
 * INPUT:
 * điểm thi 1,2,3
 * chọn đối tượng 1,2,3
 * chọn khu vực 1,2,3
 * điểm chuẩn
 *
 * TODO:
 * Gán điểm cộng thêm cho đối tượng & khu vực ưu tiên.
 * Công điểm thi 3 môn, cộng thêm điểm ưu tiên. Xét xem >= điểm chuẩn không?
 *  --> Bằng hoặc lớn hơn điểm chuẩn thì đậu
 *  --> Bé hơn thì rớt
 *
 * OUTPUT:
 * Ra kết quả Rớt/Đậu.
 */

function checkExamResult() {
  var subject1score = document.getElementById("txt-subject1").value * 1;
  var subject2score = document.getElementById("txt-subject2").value * 1;
  var subject3score = document.getElementById("txt-subject3").value * 1;
  var khuVuc = document.querySelector(
    'input[name="khu-vuc-uu-tien"]:checked'
  ).value;
  var doiTuong = document.getElementById("txt-doi-tuong-uu-tien").value;
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;

  function checkKhuVuc(khuVuc) {
    var khuA = "A";
    var khuB = "B";
    var khuC = "C";
    var khuX = "X";
    switch (khuVuc) {
      case khuA:
        return 2;
      case khuB:
        return 1.5;
      case khuC:
        return 0.5;
      default:
        return 0;
    }
  }
  function checkDoiTuong(doiTuong) {
    var doiTuong1 = 1;
    var doiTuong2 = 2;
    var doiTuong3 = 3;
    var doiTuong0 = 0;
    switch (doiTuong) {
      case doiTuong1:
        return 2.5;
      case doiTuong2:
        return 1.5;
      case doiTuong3:
        return 1;
      default:
        return 0;
    }
  }

  var diemKhuVuc = checkKhuVuc(khuVuc);
  var diemDoiTuong = checkDoiTuong(doiTuong);
  var tongDiem =
    subject1score + subject2score + subject3score + diemDoiTuong + diemKhuVuc;

  if (tongDiem < diemChuan) {
    document.getElementById(
      "ex01-result"
    ).innerHTML = `Chúc mừng bạn <span class="text-warning">Rớt</span> rồi!!!`;
  } else {
    document.getElementById(
      "ex01-result"
    ).innerHTML = `Chúc mừng bạn <span class="text-danger">Đậu</span> rồi!!!`;
  }
}
// END BÀI 1

/** BÀI 2
 *
 *  INPUT:
 * Số điện (soDien):
 *
 *  TODO:
 * nếu soDien <=50 thì đơn giá 500d / kWh cho 50kWh.
 * nếu 50 <= soDien <=100 thì đơn giá 650d / kWh cho mức 50-100 (50kWh) dưới 50 như trên
 * nếu 100 <= soDien <=200 thì đơn giá 850d / kWh cho mức 100-200 (100kWh) dưới 100 như trên
 * nếu 200 <= soDien <=350 thì đơn giá 1100d / kWh cho mức 200-350 (150kWh) dưới 200 như trên
 * nếu soDien > 350 thì đơn giá 1300d / kWh cho mức trên 350.
 *
 * soDien các phần * đơn giá để ra tongTienDien.
 *
 * OUTPUT:
 * tongTienDien
 */

function calcElectricBill() {
  var soDien = document.getElementById("txt-so-dien").value * 1;
  var mucDien1 = 50;
  var mucDien2 = 100;
  var mucDien3 = 200;
  var mucDien4 = 350;
  var giaDienBac1 = 500;
  var giaDienBac2 = 650;
  var giaDienBac3 = 850;
  var giaDienBac4 = 1100;
  var giaDienBac5 = 1300;

  var tinhTienDien = function (a) {
    var tienDien = null;
    if (a <= mucDien1) {
      tienDien = giaDienBac1 * a;
    } else if (a <= mucDien2) {
      tienDien = giaDienBac1 * mucDien1 + giaDienBac2 * (a - mucDien1);
    } else if (a <= mucDien3) {
      tienDien =
        giaDienBac1 * mucDien1 +
        giaDienBac2 * (mucDien2 - mucDien1) +
        giaDienBac3 * (a - mucDien2);
    } else if (a <= mucDien4) {
      tienDien =
        giaDienBac1 * mucDien1 +
        giaDienBac2 * (mucDien2 - mucDien1) +
        giaDienBac3 * (mucDien3 - mucDien2) +
        giaDienBac4 * (a - mucDien3);
    } else {
      tienDien =
        giaDienBac1 * mucDien1 +
        giaDienBac2 * (mucDien2 - mucDien1) +
        giaDienBac3 * (mucDien3 - mucDien2) +
        giaDienBac4 * (mucDien4 - mucDien3) +
        giaDienBac5 * (a - mucDien4);
    }
    return tienDien;
  };

  var tongTienDien = tinhTienDien(soDien);
  document.getElementById(
    "ex02-result"
  ).innerHTML = `Tổng số tiền phải trả: <span class="text-warning">${tongTienDien} VNĐ`;
}
